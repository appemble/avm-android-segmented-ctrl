package com.appemble.segmented.controls;

import java.util.Vector;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.RadioGroup;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.controls.ControlInterface;
import com.appemble.avm.controls.RADIOGROUP;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.fragment.activities.DynamicFragment;

public class SEGMENTED_CONTROL_GROUP extends RADIOGROUP implements ControlInterface {
	int iCurrentCheckedId = -1; 
	Fragment currentFragment = null;
	public SEGMENTED_CONTROL_GROUP(final Context context, final ControlModel controlObject) {
		super(context, controlObject);
	}

	OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int checkedId) { 
            View v = findViewById(checkedId);
            if (!(v instanceof SEGMENTED_BUTTON))
            	return;
            SEGMENTED_BUTTON segmentedButton = (SEGMENTED_BUTTON) v;
    		AppembleActivity appembleActivity = Utilities.getActivity(getContext(), SEGMENTED_CONTROL_GROUP.this, radioGroup);
    		if (null == appembleActivity) {
    			LogManager.logError(Constants.MISSING_ACTIVITY);
    			return;
    		}
			FragmentTransaction ft = ((FragmentActivity)appembleActivity).getSupportFragmentManager().beginTransaction();
			if (null == ft)
				return;
            if (segmentedButton.isChecked()) {
            	Fragment fragment = segmentedButton.getFragment(ft, radioGroup);
    			if (currentFragment != fragment) {
                	if (currentFragment != null)
                		ft.hide(currentFragment);
                	iCurrentCheckedId = checkedId;
    				currentFragment = fragment;
    				ft.show(currentFragment);
    			}
            }
			ft.commit();
        }
	};
	
	public Object initialize(View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		Object bReturn = initialize(parentView, fParentWidth, fParentHeight, vChildControls, 
				screenModel, SEGMENTED_BUTTON.class);		
		setOnCheckedChangeListener(onCheckedChangeListener);
		return bReturn;
	}
	
	public Fragment getCurrentFragment() {
		return currentFragment;
	}
	
	public ControlModel getControl(String sName, Integer iSource) {
        if (null == currentFragment)
            return null;
        if (!(currentFragment instanceof DynamicFragment))
        	return null;
        DynamicFragment df = (DynamicFragment)currentFragment;
        ControlModel cm = iSource == Constants.FROM_FIELD_NAME ? 
            Action.getControl(df.getRootLayout(), df.mActivityData.vControls, sName, Constants.FROM_FIELD_NAME) : 
            Action.getControl(df.getRootLayout(), df.mActivityData.vControls, sName, Constants.FROM_NAME);
        if (null != cm)
            return cm;
        return null;
	}

    public View getView(int iControlId, View parentView, 
        ControlModel parentControlModel, Vector <ControlModel> vControlsToSearch) {
        if (null == currentFragment)
            return null;
        if (!(currentFragment instanceof DynamicFragment))
        	return null;
        DynamicFragment df = (DynamicFragment)currentFragment;
        View view = AppembleActivityImpl.findViewById(iControlId, df.getRootLayout(), null, df.mActivityData.vControls); 
        if (null != view)
            return view;
        return null;
    }
}
