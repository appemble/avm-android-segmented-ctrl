package com.appemble.segmented.controls;

import java.util.Vector;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

import com.appemble.avm.LogManager;
import com.appemble.avm.controls.ControlInterface;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class TAB_CONTROL_GROUP extends SEGMENTED_CONTROL_GROUP implements ControlInterface {
  
	public TAB_CONTROL_GROUP(final Context context, final ControlModel controlObject) {
		super(context, controlObject);
	}
  
	public Object initialize(View parentView, float fParentWidth, float fParentHeight,
		Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		Boolean bReturn = (Boolean)initialize(parentView, fParentWidth, fParentHeight,
		vChildControls, screenModel, TAB_BUTTON.class);
		      
		      String sLineColor = controlModel.mExtendedProperties.get("line_color");
		if (null == sLineColor) {
		LogManager.logError("The attribute line_color is a mandatory for TAB_CONTROL_GROUP: " + controlModel.sName);
		return Boolean.valueOf(false);
		}
		int lineColor = Color.parseColor(sLineColor.trim());
		String sLineHeight = controlModel.mExtendedProperties.get("unchecked_line_height");
		if (null == sLineHeight) {
		    LogManager.logError("The attribute unchecked_line_height is a mandatory for TAB_CONTROL_GROUP: " + controlModel.sName);
		    return Boolean.valueOf(false);
		}
		int lineHeightUnselected = Integer.parseInt(sLineHeight.trim());
		sLineHeight = controlModel.mExtendedProperties.get("checked_line_height");
		if (null == sLineHeight) {
		    LogManager.logError("The attribute checked_line_height is a mandatory for TAB_CONTROL_GROUP: " + controlModel.sName);
		    return Boolean.valueOf(false);
		}
		int lineHeightSelected = Integer.parseInt(sLineHeight.trim());
		
		int iCount = this.getChildCount();
		for(int i = 0; i < iCount; i++) {
		    View view = this.getChildAt(i);
		    if (!(view instanceof TAB_BUTTON)) {
		      LogManager.logError("TAB_CONTROL_GROUP can only contains TAB_BUTTON as its child: " + controlModel.sName);
		      return Boolean.valueOf(false);
		    }
		    TAB_BUTTON btn = (TAB_BUTTON) this.getChildAt(i);
		    btn.initializeTabAttributes(lineColor, lineHeightSelected, lineHeightUnselected);
		}
      
		setOnCheckedChangeListener(onCheckedChangeListener);
		return bReturn;
  	}
}
