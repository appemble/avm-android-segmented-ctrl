package com.appemble.segmented.controls;

import java.util.Vector;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.view.View;
import android.widget.RadioGroup;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.controls.ControlInterface;
import com.appemble.avm.controls.RADIOBUTTON;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.ScreenDao;
import com.appemble.fragment.activities.DynamicFragment;

public class SEGMENTED_BUTTON extends RADIOBUTTON implements ControlInterface {
	Fragment fragment = null;
	
	public SEGMENTED_BUTTON(final Context context, final ControlModel controlObject) {
		super(context, controlObject);
	}

	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);

		if (!(parentView instanceof SEGMENTED_CONTROL_GROUP))
			return Boolean.valueOf(false);

		setButtonDrawable(new StateListDrawable()); // remove the default button appearance.
		setPadding(0, 0, 0, 0); // reset the padding to 0. By default RadioButton has a left padding.

		this.setSelected(false);
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
		AppearanceManager.getInstance().updateTextAppearance(this, controlModel, Constants.APPEARANCE_ID_NOT_SET);
		setLabel();
//		if (controlModel != null && controlModel.bActionYN) {
//			setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					ActionModel[] actions = controlModel.getActions(Constants.TAP);
//					if (null != actions)
//						for (int i = 0; i < actions.length; i++)
//							if (actions[i].iPrecedingActionId == 0) {
//								Action a = new Action();
//								a.execute(getContext(), v, parentView, actions[i]);
//							}
//				}
//			});
//		}
		return Boolean.valueOf(true);
	}
	
	public Fragment getFragment(FragmentTransaction ft, RadioGroup rg) {
		if (null != fragment)
			return fragment;
		// find the activity on which the segmented control is placed.
		AppembleActivity appembleActivity = Utilities.getActivity(getContext(), this, rg);
		if (null == appembleActivity) {
			LogManager.logError(Constants.MISSING_ACTIVITY);
			return null;
		}
		ControlModel containerControlModel = null;
		
		String sFragmentName = controlModel.mExtendedProperties.get("fragment");
		if (null == sFragmentName) {
			LogManager.logError("No fragment attached to this segemented button");
			return null;
		}
		SEGMENTED_CONTROL_GROUP scp = (SEGMENTED_CONTROL_GROUP)rg;
		String sContainer = null;
		if (null == scp || null == scp.getControlModel() || null == scp.getControlModel().mExtendedProperties)
			return null;
		sContainer = scp.getControlModel().mExtendedProperties.get("container");
		if (null == sContainer)
			return null;
		// find the control model for the container view.
		containerControlModel = AppembleActivityImpl.findControlModelByName(appembleActivity, sContainer);
		if (null == containerControlModel) {
			LogManager.logError("Not a valid control name in target");
			return null;
		}
		
		// find the container view.
		View targetView = AppembleActivityImpl.findViewInScreen(appembleActivity, containerControlModel);
		if (null == targetView) {
			LogManager.logError("Cannot find view in the screen");
			return null;
		}
		// load the screen model.
	    ScreenModel screenModel = ScreenDao.getScreenByName(controlModel.sSystemDbName, controlModel.sContentDbName, 
	    		sFragmentName);
		if (null == screenModel) {
			LogManager.logError("Cannot find screen with name = " + sContainer);
			return null;
		}
		fragment = DynamicFragment.newInstance(screenModel, containerControlModel.fWidthInPixels, 
				containerControlModel.fHeightInPixels);
		ft.add(containerControlModel.id, fragment);
		
		boolean bAddToBackStack = Utilities.parseBoolean(
				controlModel.mExtendedProperties.get("add_to_back_stack"));
		if (bAddToBackStack)
			ft.addToBackStack(null);		
		return fragment;
	}
}
