package com.appemble.segmented.controls;

import java.util.Vector;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.view.View;

import com.appemble.avm.controls.ControlInterface;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class TAB_BUTTON extends SEGMENTED_BUTTON implements ControlInterface {
	protected int mLineColor;
	protected int mLineHeightSelected;
	protected int mLineHeightUnselected;
	private Rect mRectangleSelected, mRectangleUnselected;
	private Paint mLinePaint;

	public TAB_BUTTON(final Context context, final ControlModel controlObject) {
		super(context, controlObject);
	}

	public Object initialize(final View parentView, float fParentWidth, float fParentHeight, 
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		return super.initialize(parentView, fParentWidth, fParentHeight, vChildControls, screenModel);
	}

	public Object initializeTabAttributes(int lineColor, int lineHeightSelected, int lineHeightUnselected) {
		mLineColor = lineColor;
		mLineHeightUnselected = lineHeightUnselected;
		mLineHeightSelected = lineHeightSelected;
		mLinePaint = new Paint();
		mLinePaint.setColor(mLineColor);
		mLinePaint.setStyle(Style.FILL);
		return Boolean.valueOf(true);
	}

	@Override
	public void onDraw(Canvas canvas) {
		if (mLineHeightSelected > 0 && null == mRectangleSelected)
			mRectangleSelected = new Rect(0, this.getHeight() - mLineHeightSelected, getWidth(), this.getHeight());
		if (mLineHeightUnselected > 0 && null == mRectangleUnselected)
			mRectangleUnselected = new Rect(0, this.getHeight() - mLineHeightUnselected, getWidth(), this.getHeight());

		// Draw the line
		if (mLinePaint.getColor() != 0 && (mLineHeightSelected > 0 || mLineHeightUnselected > 0)) {
			Rect rect = isChecked() ? mRectangleSelected : mRectangleUnselected;
			if (rect != null)
				canvas.drawRect(rect, mLinePaint);
		}
		super.onDraw(canvas);
	}
}
